package com.research.patientsapi.service;

import java.util.List;

import com.research.patientsapi.dto.HerbsList;
import com.research.patientsapi.models.Herb;
import com.research.patientsapi.models.Treatment;

public interface TreatmentService {

	/**
	 * get treatment
	 * @param id
	 * @return
	 */
	public Treatment getTreatmentById(long id);
	
	/**
	 * return all herbs for treatment
	 * @param treatmentId
	 * @return
	 */
	public HerbsList getAllHerbsForTreatment(long treatmentId);
	
}
