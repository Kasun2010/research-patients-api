package com.research.patientsapi.service;

import com.research.patientsapi.models.Location;

public interface LocationService {

	public Location getLocationById(long id);
}
