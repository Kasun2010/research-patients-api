package com.research.patientsapi.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.research.patientsapi.dto.HistoryList;
import com.research.patientsapi.models.History;

/**
 * 
 * @author Kasun
 *
 */
public interface HistoryService {

	/**
	 * Create new history record
	 * @param history
	 * @return
	 */
	public History createNewHistory(History history);
	
	/**
	 * Get patient history
	 * @param patientId
	 * @return
	 */
	public HistoryList getPatientHistory(long patientId);
	
	/**
	 * Update patient history
	 * @param historyId
	 * @param history
	 * @return
	 */
	public History updateHistory(long historyId,History history);
}
