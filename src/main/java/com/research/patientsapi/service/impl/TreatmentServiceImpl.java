package com.research.patientsapi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.dto.HerbsList;
import com.research.patientsapi.models.Herb;
import com.research.patientsapi.models.Treatment;
import com.research.patientsapi.repository.HerbRepository;
import com.research.patientsapi.repository.TreatmentRepository;
import com.research.patientsapi.service.TreatmentService;

/**
 * 
 * @author Kasun Gunathilaka
 *
 */
@Repository("treatmentService")
public class TreatmentServiceImpl implements TreatmentService {

	@Autowired
	TreatmentRepository repo;
	
	@Autowired
	HerbRepository herbRepo;
	
	/**
	 * get treatment by id
	 */
	@Override
	public Treatment getTreatmentById(long id) {
		Treatment dto=new Treatment();
		try {
			dto=repo.findById(id);
		} catch (Exception e) {
			throw new NotFoundException("Treatment not found");
		}
		return dto;
	}

	@Override
	public HerbsList getAllHerbsForTreatment(long treatmentId) {
       
		HerbsList list=new HerbsList();
		try {
			list.setHerbs(herbRepo.findHerbByTreatmentId(treatmentId));
		} catch (Exception e) {
			throw new NotFoundException("herbs not found for given treatment id");
		}
		
		return list;
	}

}
