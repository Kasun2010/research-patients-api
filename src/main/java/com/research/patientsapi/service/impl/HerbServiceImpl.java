package com.research.patientsapi.service.impl;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.dto.LocationList;
import com.research.patientsapi.models.Herb;
import com.research.patientsapi.models.HerbLocation;
import com.research.patientsapi.models.Location;
import com.research.patientsapi.repository.HerbLocationRepository;
import com.research.patientsapi.repository.HerbRepository;
import com.research.patientsapi.repository.LocationRepository;
import com.research.patientsapi.service.HerbService;

@Repository("herbService")
public class HerbServiceImpl implements HerbService {

	private Logger logger=LoggerFactory.getLogger(HerbServiceImpl.class);
	@Autowired
	HerbRepository repo;
	
	@Autowired
	HerbLocationRepository herbLocationRepo;
	
	@Autowired
	LocationRepository locationRepo;
	
	@Override
	public Herb getHerbById(long id) {
		Herb dto=new Herb();
		try {
			dto=repo.findHerbById(id);
		} catch (Exception e) {
			throw new NotFoundException("Herb not found");
		}
		return dto;
	}

	@Override
	public LocationList getLocationList(long id) {
		LocationList list=new LocationList();
		List<Location> locations=new ArrayList<>();
		List<HerbLocation> locationIdList=new ArrayList<>();
		try {
			locationIdList=herbLocationRepo.findAllByHerbId(id);
			for (int i = 0; i < locationIdList.size(); i++) {
				locations.add(locationRepo.findLocationById(locationIdList.get(i).getLocationId()));
			}
			
			
			list.setLocations(locations);
		} catch (Exception e) {
			throw new NotFoundException("Locations not found for given herb");
		}

		return list;
	}
	

}
