package com.research.patientsapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.models.Location;
import com.research.patientsapi.repository.LocationRepository;
import com.research.patientsapi.service.LocationService;

@Repository("locationService")
public class LocationServiceImpl implements LocationService {

	@Autowired
	LocationRepository repo;
	
	@Override
	public Location getLocationById(long id) {
		Location dto=new Location();
		try {
			dto=repo.findLocationById(id);
		} catch (Exception e) {
			throw new NotFoundException("Location is not found");
		}
		return dto;
	}

}
