package com.research.patientsapi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.models.Diet;
import com.research.patientsapi.repository.DietRepository;
import com.research.patientsapi.service.DietService;

@Repository("dietService")
public class DietServiceImpl implements DietService {

	@Autowired
	DietRepository repo;
	
	
	@Override
	public Diet getDietById(long id) {
		Diet dto=new Diet();
		try {
			dto=repo.findDietById(id);
		} catch (Exception e) {
			throw new NotFoundException("Diet not found");
		}
		return dto;
	}

}
