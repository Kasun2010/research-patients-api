package com.research.patientsapi.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.dto.PatientsList;
import com.research.patientsapi.models.Patient;
import com.research.patientsapi.repository.PatientRepository;
import com.research.patientsapi.service.PatientService;

/**
 * Patient service implementation
 * @author Kasun
 *
 */
@Repository("patientService")
public class PatientServiceImpl implements PatientService {

	private Logger logger=LoggerFactory.getLogger(PatientServiceImpl.class);
	
	@Autowired
	private PatientRepository patientRepo;
	
	/**
	 * create new patient
	 */
	@Override
	public Patient createNewPatient(Patient patient) {
		Patient newPatient=new Patient();
		
		try {
			newPatient=patientRepo.save(patient);
		} catch (Exception e) {
			logger.info("Cannot create patient"+e.getMessage());
			
		}
		return newPatient;
	}
	
	/**
	 * get pattient details for given id
	 */
	public Patient getPatient(long patientId){
		Patient patient=new Patient();
		try {
			patient=patientRepo.findById(patientId);
			if(patient==null){
				throw new NotFoundException("Patient not found for given patient id");
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		return patient;
	}

	@Override
	public PatientsList getAllPatients() {

		PatientsList patients=new PatientsList();
		try {
			patients.setPatients(patientRepo.findAll());
			if(patients==null){
				throw new NotFoundException("Unable to retrieve patients");
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		return patients;
	}

	@Override
	public Patient updatePatient(long patientId,Patient editPatient) {
		Patient patient=new Patient();
		Patient updatedPatient=new Patient();
		try {
			 patient=patientRepo.findById(patientId);
			if(patient.getId()<=0){
				throw new NotFoundException("Patient not found.Cannot update");
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		
		try {
			patient.setFirstname(editPatient.getFirstname());
			patient.setLastname(editPatient.getLastname());
			patient.setAge(editPatient.getAge());
			patient.setDob(editPatient.getDob());
			patient.setGender(editPatient.getGender());
			patient.setCivilstatus(editPatient.getCivilstatus());
			patient.setAddress1(editPatient.getAddress1());
			patient.setAddress2(editPatient.getAddress2());
			patient.setAddress3(editPatient.getAddress3());
			patient.setTitle(editPatient.getTitle());
			patient.setOccupation(editPatient.getOccupation());
			patient.setLandline(editPatient.getLandline());
			patient.setMobile(editPatient.getMobile());
			
			updatedPatient=patientRepo.save(patient);
		} catch (Exception e) {
			logger.info("Cannot update "+e.getMessage());
		}
		
	
		return updatedPatient;
	}

}
