package com.research.patientsapi.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.research.patientsapi.repository.UserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepo;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		boolean userExist;
		try {
			com.research.patientsapi.models.User user=new com.research.patientsapi.models.User();
			user=userRepo.findByUsername(username);
			if(user!=null){
				return new User(user.getUsername(), user.getPassword(),
						new ArrayList<>());
			}else{
				throw new UsernameNotFoundException("User not found with username: " + username);
			}
		} catch (Exception e) {
			throw new UsernameNotFoundException(e.getMessage());
		}

	}

}