package com.research.patientsapi.service.impl;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.dto.UsersList;
import com.research.patientsapi.models.User;
import com.research.patientsapi.repository.UserRepository;
import com.research.patientsapi.service.UserService;

//@Repository("userService")
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepo;
	private UsersList users;

	@Override
	public UsersList getAllUsers() {
		users=new UsersList();
		users.setUsers(userRepo.findAll());
		return users;
	}

	@Override
	public UserDetails loadUserByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User registerUser(User user) {
		
		BCryptPasswordEncoder encoder=new BCryptPasswordEncoder();
        try {
        	if(user==null){
        		throw new NotFoundException("user cant register");
        	}else{
        		String encoded=encoder.encode(user.getPassword());
            	user.setPassword(encoded);
    			userRepo.save(user);
        	}
        	
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		return user;
	}

	@Override
	public User getUser(String username) {
		User user=new User();
		try {
			user=userRepo.findByUsername(username);
			if(user==null){
				throw new NotFoundException("User not found");
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		return user;
	}


//	@Override
//	public UserDetails loadUserByUsername(String username) {
//		return userRepo.findByUsername(username);
//		
//	}
	
	
}
