package com.research.patientsapi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.dto.HistoryList;
import com.research.patientsapi.models.History;
import com.research.patientsapi.repository.HistoryRepository;
import com.research.patientsapi.service.HistoryService;

@Repository("historyService")
public class HistoryServiceImpl implements HistoryService {

	private Logger logger=LoggerFactory.getLogger(HistoryServiceImpl.class);
	@Autowired
	HistoryRepository histRepo;
	
	@Override
	public History createNewHistory(History history) {
		History newHistory=new History();
		try {
			
			newHistory=histRepo.save(history);
		} catch (Exception e) {
			logger.info("Cannot create history : "+e.getMessage());
		}
		return newHistory;
	}

	@Override
	public HistoryList getPatientHistory(long patientId) {
		List<History> list=new ArrayList<>();
		HistoryList history=new HistoryList();
		try {
			list=histRepo.findByPatientId(patientId);
			history.setHistory(list);
			if(list==null){
				throw new NotFoundException("No histories found for the given patient");
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		return history;
	}

	@Override
	public History updateHistory(long historyId, History history) {
		
		History editHistory=new History();
		History updatedHistory=new History();
		
		try {
			editHistory=histRepo.findById(historyId);
			if(editHistory.getId()<=0){
				throw new NotFoundException("History details not found. Cannot update");
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		
		try {
			editHistory.setAngina(history.getAngina());
			editHistory.setBloodpressure(history.getBloodpressure());
			editHistory.setBloodsugar(history.getBloodsugar());
			editHistory.setChestpain(history.getChestpain());
			editHistory.setChestpaindetails(history.getChestpaindetails());
			editHistory.setCholestrol(history.getCholestrol());
			editHistory.setDepression(history.getDepression());
			editHistory.setEcg(history.getEcg());
			editHistory.setHeartrate(history.getHeartrate());
			editHistory.setPeakexercise(history.getPeakexercise());
			editHistory.setThalassemia(history.getThalassemia());
			
			updatedHistory=histRepo.save(editHistory);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return updatedHistory;
	}

}
