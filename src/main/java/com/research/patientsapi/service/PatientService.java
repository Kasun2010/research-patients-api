package com.research.patientsapi.service;

import java.util.List;

import com.research.patientsapi.dto.PatientsList;
import com.research.patientsapi.models.Patient;

/**
 * 
 * @author Kasun
 *
 */
public interface PatientService {

	/**
	 * Create new patient record
	 * @param patient
	 * @return
	 */
	public Patient createNewPatient(Patient patient);
	
	/**
	 * Get single patient
	 * @param patientId
	 * @return
	 */
	public Patient getPatient(long patientId);
	
	/**
	 * Get all patients
	 * @return
	 */
	public PatientsList getAllPatients();
	
	public Patient updatePatient(long patientId,Patient editPatient);
}
