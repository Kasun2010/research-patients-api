package com.research.patientsapi.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.research.patientsapi.dto.UsersList;
import com.research.patientsapi.models.User;

@Service
public interface UserService {

	public UsersList getAllUsers();
	public UserDetails loadUserByUsername(String username);
	public User registerUser(User user);
	public User getUser(String username);
}
