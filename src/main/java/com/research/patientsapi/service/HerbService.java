package com.research.patientsapi.service;

import com.research.patientsapi.dto.LocationList;
import com.research.patientsapi.models.Herb;

public interface HerbService {

	public Herb getHerbById(long id);
	
	public LocationList getLocationList(long id);
	
}
