package com.research.patientsapi.service;

import com.research.patientsapi.models.Diet;

public interface DietService {

	public Diet getDietById(long id);
}
