package com.research.patientsapi.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 
 * @author Kasun Gunathilaka
 *
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends Exception {

	 /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -554381281953166386L;

    /**
     * Instantiates a new note not found exception.
     *
     * @param message
     *            the message
     */
    public NotFoundException(final String message) {
        super(HttpStatus.NOT_FOUND.value(), message);
    }
}
