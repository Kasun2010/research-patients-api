package com.research.patientsapi.Exceptions;

public class Exception extends RuntimeException {

	 /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4836827547108110089L;

    /** The code. */
    private int status;

    /**
     * Instantiates a new note exception.
     *
     * @param code
     *            the code
     * @param message
     *            the message
     */
    public Exception(final int status, final String message) {
        super(message);
        this.status = status;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the code to set
     */
    public void setStatus(final int status) {
        this.status = status;
    }
}
