package com.research.patientsapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.research.patientsapi.models.Location;

public interface LocationRepository extends JpaRepository<Location, Long>{

	public Location findLocationById(long id);
}
