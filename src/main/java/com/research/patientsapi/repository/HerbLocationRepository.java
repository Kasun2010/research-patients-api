package com.research.patientsapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.research.patientsapi.models.Diet;
import com.research.patientsapi.models.HerbLocation;

public interface HerbLocationRepository extends JpaRepository<HerbLocation, Long>{

	public List<HerbLocation> findAllByHerbId(long herbId);
}
