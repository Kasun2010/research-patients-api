package com.research.patientsapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.research.patientsapi.models.History;


public interface HistoryRepository extends JpaRepository<History, Long> {

//	@Query(value="SELECT h FROM history h WHERE h.patient_id=?1")
	/**
	 * 
	 * @param patientId
	 * @return
	 */
	public List<History> findByPatientId(long patientId);
	
	/**
	 * get a single history
	 * @param historyId
	 * @return
	 */
	public History findById(long historyId);
}
