package com.research.patientsapi.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.research.patientsapi.models.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {

	Patient findById(long patientId);
   
}
