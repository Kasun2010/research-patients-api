package com.research.patientsapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.research.patientsapi.dto.HerbsList;
import com.research.patientsapi.models.Herb;

public interface HerbRepository extends JpaRepository<Herb, Long> {

	public Herb findHerbById(long id);
	
	public List<Herb> findHerbByTreatmentId(long treatmentId);
	
}
