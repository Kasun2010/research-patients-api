package com.research.patientsapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.research.patientsapi.models.Diet;

public interface DietRepository extends JpaRepository<Diet, Long> {

	public Diet findDietById(long id);
}
