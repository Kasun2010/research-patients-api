package com.research.patientsapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.research.patientsapi.models.Treatment;

public interface TreatmentRepository extends JpaRepository<Treatment, Long> {
  
	/**
	 * Get treatment
	 * @param id
	 * @return
	 */
	public Treatment findById(long id);
}
