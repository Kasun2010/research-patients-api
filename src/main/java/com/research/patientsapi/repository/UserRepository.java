package com.research.patientsapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import com.research.patientsapi.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

//	public UserDetails findByUsername(String username);
	public User findByUsername(String username);
}
