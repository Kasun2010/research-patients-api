package com.research.patientsapi.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.models.Diet;
import com.research.patientsapi.service.DietService;

@RestController
@RequestMapping(value = "/diet")
public class DietController {

	@Autowired
	DietService dservice;
	
	@RequestMapping(value="/{id}",method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	 public ResponseEntity<Diet> getDiet(@PathVariable(value="id") long dietId){
		 try {
			Diet diet=dservice.getDietById(dietId);
			if(diet==null){
				throw new NotFoundException("Requested herb not found");
			}else{
				 return new ResponseEntity<Diet>(diet,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		 
	 }
}
