package com.research.patientsapi.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.research.patientsapi.Exceptions.ExceptionMessage;
import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.dto.PatientCreatedDTO;
import com.research.patientsapi.dto.PatientMedicalRecordDTO;
import com.research.patientsapi.dto.PatientsList;
import com.research.patientsapi.models.History;
import com.research.patientsapi.models.Patient;
import com.research.patientsapi.service.HistoryService;
import com.research.patientsapi.service.PatientService;

/**
 * 
 * @author Kasun
 *
 */
@RestController
//@CrossOrigin("http://localhost:4200")
@RequestMapping(value = "/patients")
public class PatientController  {

	private Logger logger=LoggerFactory.getLogger(PatientController.class);
	
	@Autowired
	PatientService patientService;
	
	@Autowired
	HistoryService histService;
	
	/**
	 * create a new patient
	 * @param patient
	 * @return
	 */
	 @RequestMapping(method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	 public ResponseEntity<ExceptionMessage> createNewpatient(@RequestBody PatientMedicalRecordDTO patient){
		 
		 /**
		  * new objects for keep body data
		  */
		 Patient newPatient=new Patient();
		 History newHistory=new History();
		 
		 /**
		  * service callers
		  */
		 newPatient=patientService.createNewPatient(patient.getPersonalInfo());
		 patient.getMedicalInfo().setPatientId(newPatient.getId());
		 newHistory=histService.createNewHistory(patient.getMedicalInfo());
		 
		 /**
		  * error handling
		  */
		 if(newPatient.getId()<=0 || newHistory.getId()<=0){
			 
			 return new ResponseEntity<ExceptionMessage>(new ExceptionMessage(HttpStatus.BAD_REQUEST.value(), "Patient creation unsuccessfull"),HttpStatus.BAD_REQUEST );
		 }else{
			 PatientCreatedDTO dto=new PatientCreatedDTO();
			 dto.setCode(HttpStatus.CREATED.value());
			 dto.setMessage("Patient created successfully");
			 dto.setPatientId(newPatient.getId());
			 return new ResponseEntity<ExceptionMessage>(dto,HttpStatus.CREATED );
		 }

	 }
	 
	 /**
	  * Get single patient for given id
	  * @param patientId
	  * @return
	  */
	 @RequestMapping(value="/{patientId}",method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	 public ResponseEntity<Patient> getPatient(@PathVariable long patientId){
		 try {
			Patient patient=patientService.getPatient(patientId);
			if(patient==null){
				throw new NotFoundException("Requested patient not found");
			}else{
				 return new ResponseEntity<Patient>(patient,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		 
	 }
	 
	 /**
	  * List all patients
	  * @return
	  */
	 @RequestMapping(method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	 public ResponseEntity<PatientsList> getAllPatients(){
		 PatientsList patients=new PatientsList();
		 try {
			patients=patientService.getAllPatients();
			 return new ResponseEntity<PatientsList>(patients,HttpStatus.OK);
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
	 }
	 
	 /**
	  * Update patient, history instance
	  * @param patientId
	  * @param patient
	  * @return
	  */
	 @RequestMapping(value="/{patientId}",method = RequestMethod.PUT, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	 public ResponseEntity<ExceptionMessage> updatePatient(@PathVariable(value="patientId") long patientId, @RequestBody PatientMedicalRecordDTO patient){
		 Patient newPatient=new Patient();
		 History newHistory=new History();
		 
		 
		 try {
			newPatient=patientService.updatePatient(patientId, patient.getPersonalInfo());

		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		 try {
			newHistory=histService.updateHistory(patient.getMedicalInfo().getId(), patient.getMedicalInfo());
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
			if(newPatient.getId()<=0 || newHistory.getId()<=0){
				 return new ResponseEntity<ExceptionMessage>(new ExceptionMessage(HttpStatus.BAD_REQUEST.value(), "Patient update unsuccessfull"),HttpStatus.BAD_REQUEST );
			}else{
				PatientCreatedDTO dto=new PatientCreatedDTO();
				 dto.setCode(HttpStatus.CREATED.value());
				 dto.setMessage("Patient updated successfully");
				 dto.setPatientId(newPatient.getId());
				 return new ResponseEntity<ExceptionMessage>(dto,HttpStatus.CREATED );
			}
	 }
}
