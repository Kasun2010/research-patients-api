package com.research.patientsapi.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.research.patientsapi.Exceptions.ExceptionMessage;
import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.config.JwtTokenUtil;
import com.research.patientsapi.dto.UsersList;
import com.research.patientsapi.models.JwtRequest;
import com.research.patientsapi.models.JwtResponse;
import com.research.patientsapi.models.User;
import com.research.patientsapi.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	

    @Autowired
    private UserDetailsService jwtUserDetailsService;


	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	
	   public UserController() {
	   }
	   
	 @RequestMapping(method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	 public  HttpEntity<UsersList>getAllUsers(){
		 return new ResponseEntity<UsersList>(userService.getAllUsers(),HttpStatus.OK);
	 } 
	 
	 @RequestMapping(value = "/authenticate", method = RequestMethod.POST,produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
		public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest)
				throws Exception {
		 
		   String token=null;
		   UserDetails userDetails=null;
		   User user=new User();
			
			try {
				authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}
			
			try {
				userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}
			
			try {
				user=userService.getUser(authenticationRequest.getUsername());
				token = jwtTokenUtil.generateToken(userDetails);
				
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}
			return ResponseEntity.ok(new JwtResponse(token,user.getUsername(),user.getRole()));
			
		}

		private void authenticate(String username, String password) throws Exception {
			Objects.requireNonNull(username);
			Objects.requireNonNull(password);

			try {
				authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			} catch (DisabledException e) {
				throw new Exception("USER_DISABLED", e);
			} catch (BadCredentialsException e) {
				throw new Exception("INVALID_CREDENTIALS", e);
			}
		}
		
		@RequestMapping(value = "/register",method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
		public ResponseEntity<ExceptionMessage> registerNewUser(@RequestBody User user){
			User newUser=new User();
			try {
				newUser=userService.registerUser(user);
				if(newUser.getId()<=0){
					return new ResponseEntity<ExceptionMessage>(new ExceptionMessage(HttpStatus.BAD_REQUEST.value(), "User creation unsuccessfull"),HttpStatus.BAD_REQUEST );
				}else{
					ExceptionMessage message=new ExceptionMessage();
					message.setCode(HttpStatus.CREATED.value());
					message.setMessage("User successfully created");
					 return new ResponseEntity<ExceptionMessage>(message,HttpStatus.CREATED );
				}
			} catch (Exception e) {
				throw new NotFoundException(e.getMessage());
			}
		}
}
