package com.research.patientsapi.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.models.Location;
import com.research.patientsapi.service.LocationService;

/**
 * 
 * @author Kasun Gunathilaka
 *
 */
@RestController
@RequestMapping(value = "/location")
public class LocationController {

	@Autowired
	LocationService lservice;
	
	 @RequestMapping(value="/{id}",method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	 public ResponseEntity<Location> getLocation(@PathVariable(value="id") long locationId){
		 try {
			Location location=lservice.getLocationById(locationId);
			if(location==null){
				throw new NotFoundException("Requested location not found");
			}else{
				 return new ResponseEntity<Location>(location,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		 
	 }
}
