package com.research.patientsapi.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.dto.HerbsList;
import com.research.patientsapi.dto.PatientsList;
import com.research.patientsapi.models.Treatment;
import com.research.patientsapi.service.TreatmentService;

/**
 * 
 * @author Kasun Gunathilaka
 *
 */
@RestController
@RequestMapping(value = "/treatments")
public class TreatmentController {

	@Autowired
	TreatmentService tservice;
	
	/**
	 * retrieve treatment by id
	 * @param treatmentId
	 * @return
	 */
	 @RequestMapping(value="{id}",method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	 public ResponseEntity<Treatment> getTreatment(@PathVariable(value="id") long treatmentId){
		 try {
			Treatment treatment=tservice.getTreatmentById(treatmentId);
			if(treatment==null){
				throw new NotFoundException("Requested treatment not found");
			}else{
				 return new ResponseEntity<Treatment>(treatment,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		 
   }
	 
	 /**
	  * retrieve herblist for treatment
	  * @param treatmentId
	  * @return
	  */
	 @RequestMapping(value="{id}/herbslist",method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	 public ResponseEntity<HerbsList> getAllHerbs(@PathVariable(value="id") long treatmentId){
		 HerbsList herbList=new HerbsList();
		 try {
			 herbList=tservice.getAllHerbsForTreatment(treatmentId);
			 return new ResponseEntity<HerbsList>(herbList,HttpStatus.OK);
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
	 }
	 
}
