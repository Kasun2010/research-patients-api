package com.research.patientsapi.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.dto.HistoryList;
import com.research.patientsapi.models.History;
import com.research.patientsapi.service.HistoryService;


@RestController
@RequestMapping(value = "/history")
public class HistoryController {

	@Autowired
	HistoryService historyService;
	
	/**
	 * get patient history
	 * @param patientId
	 * @return
	 */
	 @RequestMapping(value="/{patientId}",method = RequestMethod.GET,produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<HistoryList> getAllHistory(@PathVariable long patientId){
		 HistoryList list=new HistoryList();
		 try {
			list=historyService.getPatientHistory(patientId);
			return new ResponseEntity<HistoryList>(list,HttpStatus.OK);
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
	 }
	
}
