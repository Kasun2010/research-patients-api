package com.research.patientsapi.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.research.patientsapi.Exceptions.NotFoundException;
import com.research.patientsapi.dto.HerbsList;
import com.research.patientsapi.dto.LocationList;
import com.research.patientsapi.models.Herb;
import com.research.patientsapi.service.HerbService;

@RestController
@RequestMapping(value = "/herbs")
public class HerbController {

	@Autowired
	HerbService hservice;
	
	@RequestMapping(value="/{id}",method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	 public ResponseEntity<Herb> getHerb(@PathVariable(value="id") long herbId){
		 try {
			Herb herb=hservice.getHerbById(herbId);
			if(herb==null){
				throw new NotFoundException("Requested herb not found");
			}else{
				 return new ResponseEntity<Herb>(herb,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		 
	 }
	
	 @RequestMapping(value="{id}/locationlist",method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	 public ResponseEntity<LocationList> getAllLocations(@PathVariable(value="id") long herbId){
		 LocationList locationList=new LocationList();
		 try {
			 locationList=hservice.getLocationList(herbId);
			 return new ResponseEntity<LocationList>(locationList,HttpStatus.OK);
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
	 }
}
