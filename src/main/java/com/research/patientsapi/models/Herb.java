package com.research.patientsapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="herb")
public class Herb {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="sanskrith_name")
	private String sanskrithName;
	
	@Column(name="sinhala_name")
	private String sinhalaName;
	
	@Column(name="tamil_name")
	private String tamilName;
	
	@Column(name="medicinal_values")
	private String medicinalValues;
	
//	@Column(name="location_id")
//	private long locationId;
	
	@Column(name="photo")
	private String photo;
	
	@Column(name="treatment_id")
	private long treatmentId;

	
	
	public Herb() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSanskrithName() {
		return sanskrithName;
	}

	public void setSanskrithName(String sanskrithName) {
		this.sanskrithName = sanskrithName;
	}

	public String getSinhalaName() {
		return sinhalaName;
	}

	public void setSinhalaName(String sinhalaName) {
		this.sinhalaName = sinhalaName;
	}

	public String getTamilName() {
		return tamilName;
	}

	public void setTamilName(String tamilName) {
		this.tamilName = tamilName;
	}

	public String getMedicinalValues() {
		return medicinalValues;
	}

	public void setMedicinalValues(String medicinalValues) {
		this.medicinalValues = medicinalValues;
	}

//	public long getLocationId() {
//		return locationId;
//	}
//
//	public void setLocationId(long locationId) {
//		this.locationId = locationId;
//	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public long getTreatmentId() {
		return treatmentId;
	}

	public void setTreatmentId(long treatmentId) {
		this.treatmentId = treatmentId;
	}
	
	
	
}
