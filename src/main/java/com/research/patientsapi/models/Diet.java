package com.research.patientsapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="diet")
public class Diet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="description")
	private String description;
	
	@Column(name="body")
	private String body;
	
	@Column(name="diet")
	private String diet;
	
	@Column(name="grains")
	private String grains;
	
	@Column(name="fruits")
	private String fruits;
	
	@Column(name="vegetables")
	private String vegetables;
	
	@Column(name="oils")
	private String oils;

	@Column(name="sweetener")
	private String sweetener;
	
	@Column(name="dairy_products")
	private String dairyProducts;
	
	@Column(name="spices")
	private String spices;

	
	public Diet() {
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getDiet() {
		return diet;
	}

	public void setDiet(String diet) {
		this.diet = diet;
	}

	public String getGrains() {
		return grains;
	}

	public void setGrains(String grains) {
		this.grains = grains;
	}

	public String getFruits() {
		return fruits;
	}

	public void setFruits(String fruits) {
		this.fruits = fruits;
	}

	public String getVegetables() {
		return vegetables;
	}

	public void setVegetables(String vegetables) {
		this.vegetables = vegetables;
	}

	public String getOils() {
		return oils;
	}

	public void setOils(String oils) {
		this.oils = oils;
	}

	public String getSweetener() {
		return sweetener;
	}

	public void setSweetener(String sweetener) {
		this.sweetener = sweetener;
	}

	public String getDairyProducts() {
		return dairyProducts;
	}

	public void setDairyProducts(String dairyProducts) {
		this.dairyProducts = dairyProducts;
	}

	public String getSpices() {
		return spices;
	}

	public void setSpices(String spices) {
		this.spices = spices;
	}
	
	
}
