package com.research.patientsapi.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="herb_location")
public class HerbLocation {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="herb_id")
	private long herbId;
	
	@Column(name="location_id")
	private long locationId;
	

	
	public HerbLocation() {
	}

	public long getHerbId() {
		return herbId;
	}

	public void setHerbId(long herbId) {
		this.herbId = herbId;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}


	
	
}
