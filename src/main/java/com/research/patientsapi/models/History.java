package com.research.patientsapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="history")
public class History {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="patient_id")
	private long patientId;
	
	@Column(name="chestpain")
	private String chestpain;
	
	@Column(name="chestpaindesc")
	private String chestpaindetails;
	
	@Column(name="depression")
	private String depression;
	
	@Column(name="blood_pressure")
	private String bloodpressure;
	
	@Column(name="cholestrol")
	private String cholestrol;
	
	@Column(name="blood_sugar")
	private String bloodsugar;
	
	@Column(name="heart_rate")
	private String heartrate;
	
	@Column(name="angina")
	private String angina;
	
	@Column(name="thalassemia")
	private String thalassemia;
	
	@Column(name="ecg")
	private String ecg;
	
	@Column(name="peakexercise")
	private String peakexercise;

	
	public History() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public String getChestpain() {
		return chestpain;
	}

	public void setChestpain(String chestpain) {
		this.chestpain = chestpain;
	}

	public String getChestpaindetails() {
		return chestpaindetails;
	}

	public void setChestpaindetails(String chestpaindetails) {
		this.chestpaindetails = chestpaindetails;
	}

	public String getDepression() {
		return depression;
	}

	public void setDepression(String depression) {
		this.depression = depression;
	}

	public String getBloodpressure() {
		return bloodpressure;
	}

	public void setBloodpressure(String bloodpressure) {
		this.bloodpressure = bloodpressure;
	}

	public String getCholestrol() {
		return cholestrol;
	}

	public void setCholestrol(String cholestrol) {
		this.cholestrol = cholestrol;
	}

	public String getBloodsugar() {
		return bloodsugar;
	}

	public void setBloodsugar(String bloodsugar) {
		this.bloodsugar = bloodsugar;
	}

	public String getHeartrate() {
		return heartrate;
	}

	public void setHeartrate(String heartrate) {
		this.heartrate = heartrate;
	}

	public String getAngina() {
		return angina;
	}

	public void setAngina(String angina) {
		this.angina = angina;
	}

	public String getThalassemia() {
		return thalassemia;
	}

	public void setThalassemia(String thalassemia) {
		this.thalassemia = thalassemia;
	}

	public String getEcg() {
		return ecg;
	}

	public void setEcg(String ecg) {
		this.ecg = ecg;
	}

	public String getPeakexercise() {
		return peakexercise;
	}

	public void setPeakexercise(String peakexercise) {
		this.peakexercise = peakexercise;
	}
	
	
}
