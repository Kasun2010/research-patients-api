package com.research.patientsapi.dto;

import com.research.patientsapi.Exceptions.ExceptionMessage;

public class PatientCreatedDTO extends ExceptionMessage {

	private int code;
	private String message;
	private long patientId;
	
	
	public PatientCreatedDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PatientCreatedDTO(int code, String message) {
		super(code, message);
		// TODO Auto-generated constructor stub
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getPatientId() {
		return patientId;
	}
	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	
	
}
