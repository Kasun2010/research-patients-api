package com.research.patientsapi.dto;

import java.util.List;

import com.research.patientsapi.models.Herb;

public class HerbsList {

	private List<Herb> herbs;
	
	

	public HerbsList() {
	
	}

	public Iterable<Herb> getHerbs() {
		return herbs;
	}

	public void setHerbs(List<Herb> herbs) {
		this.herbs = herbs;
	}
	
	
}
