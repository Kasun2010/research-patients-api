package com.research.patientsapi.dto;

import java.util.List;

import com.research.patientsapi.models.Patient;

public class PatientsList {

	private List<Patient> patients;

	
	public PatientsList() {

	}

	public Iterable<Patient> getPatients() {
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}
	
	
}
