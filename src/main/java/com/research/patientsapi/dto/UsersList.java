package com.research.patientsapi.dto;

import java.util.List;

import com.research.patientsapi.models.User;

public class UsersList {

	private Iterable<User> users;
	
	

	public UsersList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Iterable<User> getUsers() {
		return users;
	}

	public void setUsers(Iterable<User> users) {
		this.users = users;
	}
	
	
}
