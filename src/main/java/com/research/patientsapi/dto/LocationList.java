package com.research.patientsapi.dto;

import java.util.List;

import com.research.patientsapi.models.Location;

public class LocationList {

	private List<Location> locations;
	
	

	public LocationList() {
	}

	public Iterable<Location> getLocations() {
		return locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}
	
	
}
