package com.research.patientsapi.dto;

import com.research.patientsapi.models.History;
import com.research.patientsapi.models.Patient;

public class PatientMedicalRecordDTO {

	private Patient personalInfo;
	private History medicalInfo;
	
	
	public PatientMedicalRecordDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Patient getPersonalInfo() {
		return personalInfo;
	}
	public void setPersonalInfo(Patient personalInfo) {
		this.personalInfo = personalInfo;
	}
	public History getMedicalInfo() {
		return medicalInfo;
	}
	public void setMedicalInfo(History medicalInfo) {
		this.medicalInfo = medicalInfo;
	}
	
	
}
