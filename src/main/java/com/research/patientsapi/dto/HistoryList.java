package com.research.patientsapi.dto;

import java.util.List;

import com.research.patientsapi.models.History;

public class HistoryList {

	private List<History> history;
	

	public HistoryList() {

	}

	public List<History> getHistory() {
		return history;
	}

	public void setHistory(List<History> history) {
		this.history = history;
	}
	
	
}
